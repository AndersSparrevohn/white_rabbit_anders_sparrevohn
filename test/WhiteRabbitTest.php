<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit;

class WhiteRabbitTest extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit */
    private $whiteRabbit;

    public function setUp()
    {
        $this->whiteRabbit = new WhiteRabbit();
        parent::setUp();
    }

    //SECTION FILE !
    /**
     * @dataProvider medianProvider
     */
    public function testMedian($expected, $file){
        $result = $this->whiteRabbit->findMedianLetterInFile($file);
        $this->assertTrue(in_array($result, $expected));
    }

    public function medianProvider(){
        /**
         * Test case 3 and 5 simply don't make any sense:
         * Test case 3 wants you to show 2 numbers, which aren't the medians
         * they are respective spot 11 and 12 instead of 13 and 14, which are the median numbers
         * Test case 5 wants to have the letter which has the lowest occurrence
         */
        return array(
            array(array(array("letter" => "m", "count" => 9240), array("letter" => "f", "count" => 9095)), __DIR__ ."/../txt/text1.txt"),
            array(array(array("letter" => "w", "count" => 13333), array("letter" => "m", "count" => 12641)), __DIR__ ."/../txt/text2.txt"),
            array(array(array("letter" => "w", "count" => 2227), array("letter" => "g", "count" => 2187)), __DIR__ ."/../txt/text3.txt"),
            array(array(array("letter" => "w", "count" => 3049)), __DIR__ ."/../txt/text4.txt"),
            array(array(array("letter" => "z", "count" => 858)), __DIR__ ."/../txt/text5.txt")
        );
    }
}
