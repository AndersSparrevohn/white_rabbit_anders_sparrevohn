<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    /**
     * String are allowed causing it to fail
     * Float results will always fail due to it rounding it the end
     * Causing it to never reach the loop can also cause it to fail
     */
    public function multiplyProvider(){
        return array(
            array("foobar", "foo", "bar"),
            array(700, 7, 100),
            array(42.5, 2.5, 17),
            array(2.6, 2, 1.3)
        );
    }
}
