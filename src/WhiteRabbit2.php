<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $coinsArray = array(
            '1'     => 0,
            '2'     => 0,
            '5'     => 0,
            '10'    => 0,
            '20'    => 0,
            '50'    => 0,
            '100'   => 0);


        // Loops until no money are left and checks for size of amount
        while($amount != 0) {
            if($amount >= 100) {
                $coinsArray['100']++;
                $amount -= 100;
            }
            elseif($amount >= 50) {
                $coinsArray['50']++;
                $amount -= 50;
            }
            elseif($amount >= 20) {
                $coinsArray['20']++;
                $amount -= 20;
            }
            elseif($amount >= 10) {
                $coinsArray['10']++;
                $amount -= 10;
            }
            elseif($amount >= 5) {
                $coinsArray['5']++;
                $amount -= 5;
            }
            elseif($amount >= 2) {
                $coinsArray['2']++;
                $amount -= 2;
            }
            elseif($amount >= 1) {
                $coinsArray['1']++;
                $amount -= 1;
            }
        }
        return $coinsArray;
    }
}