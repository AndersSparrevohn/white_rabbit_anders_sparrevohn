<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        // Reads the file and makes a lower case string out of it
        if(file_exists($filePath)) {
            $textString = strtolower(file_get_contents($filePath));
            return $textString;
        }
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $occurrenceArray = array("a"=>0, "b"=>0, "c"=>0, "d"=>0,
                                "e"=>0, "f"=>0, "g"=>0, "h"=>0,
                                "i"=>0, "j"=>0, "k"=>0, "l"=>0,
                                "m"=>0, "n"=>0, "o"=>0, "p"=>0,
                                "q"=>0, "r"=>0, "s"=>0, "t"=>0,
                                "u"=>0, "v"=>0, "w"=>0, "x"=>0,
                                "y"=>0, "z"=>0);

        // Checking the file for each key and then assigning the counted amount as the value
        foreach($occurrenceArray as $key => $value) {
            $occurrenceArray[$key] = substr_count($parsedFile, $key);
        }

        // Sorting the array and returning the median
        asort($occurrenceArray);
        $midSpot = (count($occurrenceArray)/2)-1;
        $charOccurrences = array_values($occurrenceArray);
        $occurrences = $charOccurrences[$midSpot];
        return array_search($occurrences, $occurrenceArray);
    }
}